import org.junit.Assert;
import org.junit.Test;
import pl.kleszczow.virustotalscanner.api.VirusTotalAPI;
import pl.kleszczow.virustotalscanner.api.VirusTotalErrorCode;
import pl.kleszczow.virustotalscanner.api.VirusTotalPreparedStatement;
import pl.kleszczow.virustotalscanner.api.VirusTotalResult;
import pl.kleszczow.virustotalscanner.api.VirusTotalStatementType;
import pl.kleszczow.virustotalscanner.api.impl.result.VirusTotalErrorResult;
import pl.kleszczow.virustotalscanner.api.impl.statement.VirusTotalMD5Scan;

public class VirusTotalMD5ScanTest
{
	@Test
	public void MD5ScanTest()
	{
		VirusTotalPreparedStatement<String> statement = new VirusTotalMD5Scan();
		VirusTotalResult result = statement.process("smthng", "smthng");
		Assert.assertSame("API should be not accessed.", result.value(), VirusTotalErrorCode.FORBIDDEN_ACCESS);
	}

	@Test
	public void MD5ScanWithAPIClass()
	{
		VirusTotalPreparedStatement preparedStatement = VirusTotalAPI.prepareStatement(VirusTotalStatementType.MD5Scan);
		VirusTotalResult result = VirusTotalAPI.processStatement(preparedStatement, "Value");
		VirusTotalPreparedStatement preparedStatement2 = VirusTotalAPI.prepareStatement(VirusTotalStatementType.MD5Scan);
		VirusTotalResult result2 = VirusTotalAPI.processStatement(preparedStatement2, "Value");
		VirusTotalPreparedStatement preparedStatement3 = VirusTotalAPI.prepareStatement(VirusTotalStatementType.MD5Scan);
		VirusTotalResult result3 = VirusTotalAPI.processStatement(preparedStatement3, "Value");
		VirusTotalPreparedStatement preparedStatement4 = VirusTotalAPI.prepareStatement(VirusTotalStatementType.MD5Scan);
		VirusTotalResult result4 = VirusTotalAPI.processStatement(preparedStatement4, "Value");
		VirusTotalPreparedStatement preparedStatement5 = VirusTotalAPI.prepareStatement(VirusTotalStatementType.MD5Scan);
		VirusTotalResult result5 = VirusTotalAPI.processStatement(preparedStatement5, "Value");
		if (result5 instanceof VirusTotalErrorResult)
		{
			System.out.println(result5.value().toString());
		}

	}
}
