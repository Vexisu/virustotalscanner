import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.junit.Assert;
import org.junit.Test;
import pl.kleszczow.virustotalscanner.api.RestConnector;

public class RestConnectorTest
{

	@Test
	public void restConnectionTest(){
		boolean test = false;
		RestConnector restConnector = new RestConnector();
		try
		{
			URL url = new URL("https://www.virustotal.com/vtapi/v2/file/report?apikey=<apikey>&resource=<resource>");
			System.out.println(restConnector.doRequest(url));
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			test = true;
		}
		Assert.assertTrue("Connector should throw exception", test);
	}
}
