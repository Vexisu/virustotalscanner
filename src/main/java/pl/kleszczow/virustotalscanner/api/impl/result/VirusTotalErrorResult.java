package pl.kleszczow.virustotalscanner.api.impl.result;

import pl.kleszczow.virustotalscanner.api.VirusTotalErrorCode;
import pl.kleszczow.virustotalscanner.api.VirusTotalResult;

public class VirusTotalErrorResult implements VirusTotalResult<VirusTotalErrorCode>
{
	private VirusTotalErrorCode errorCode;

	public VirusTotalErrorResult(VirusTotalErrorCode errorCode)
	{
		this.errorCode = errorCode;
	}

	@Override
	public VirusTotalErrorCode value()
	{
		return errorCode;
	}
}
