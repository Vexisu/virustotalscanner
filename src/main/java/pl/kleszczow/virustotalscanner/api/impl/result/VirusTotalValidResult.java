package pl.kleszczow.virustotalscanner.api.impl.result;

import pl.kleszczow.virustotalscanner.api.VirusTotalResult;

public class VirusTotalValidResult implements VirusTotalResult<String>
{
	private String value;

	public VirusTotalValidResult(String value)
	{
		this.value = value;
	}

	@Override
	public String value()
	{
		return value;
	}
}
