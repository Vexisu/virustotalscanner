package pl.kleszczow.virustotalscanner.api.impl.statement;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import pl.kleszczow.virustotalscanner.api.RestConnector;
import pl.kleszczow.virustotalscanner.api.VirusTotalErrorCode;
import pl.kleszczow.virustotalscanner.api.VirusTotalPreparedStatement;
import pl.kleszczow.virustotalscanner.api.VirusTotalResult;
import pl.kleszczow.virustotalscanner.api.impl.result.VirusTotalErrorResult;
import pl.kleszczow.virustotalscanner.api.impl.result.VirusTotalValidResult;

public class VirusTotalMD5Scan implements VirusTotalPreparedStatement<String>
{
	private static final String API_URL = "https://www.virustotal.com/vtapi/v2/file/report?apikey=<apikey>&resource=<resource>";

	public VirusTotalResult process(String value, String apiKey)
	{
		VirusTotalResult virusTotalResult = null;
		RestConnector restConnector = new RestConnector();
		try
		{
			String modifiedApiUrl = API_URL.replace("<apikey>", apiKey).replace("<resource>", value);
			URL url = new URL(modifiedApiUrl);
			virusTotalResult = new VirusTotalValidResult(restConnector.doRequest(url));
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			try
			{
				VirusTotalErrorCode errorCode = VirusTotalErrorCode.getByValue(restConnector.getErrorCode());
				return new VirusTotalErrorResult(errorCode);
			}
			catch (IOException e1)
			{
				return new VirusTotalErrorResult(VirusTotalErrorCode.NO_CONNECTION);
			}
		} finally
		{
			restConnector.disconnect();
		}
		return virusTotalResult;
	}
}
