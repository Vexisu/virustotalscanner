package pl.kleszczow.virustotalscanner.api;

import pl.kleszczow.virustotalscanner.api.impl.statement.VirusTotalMD5Scan;

public enum VirusTotalStatementType
{
	MD5Scan
	{
		@Override
		public VirusTotalPreparedStatement getStatement()
		{
			return new VirusTotalMD5Scan();
		}
	};

	public abstract VirusTotalPreparedStatement getStatement();
}
