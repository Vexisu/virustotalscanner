package pl.kleszczow.virustotalscanner.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class RestConnector
{
	private String response;
	private HttpsURLConnection serviceConnection;

	public String doRequest(URL url) throws IOException
	{
		this.serviceConnection = (HttpsURLConnection) url.openConnection();
		InputStreamReader inputStreamReader = new InputStreamReader(this.serviceConnection.getInputStream());
		if (this.serviceConnection.getResponseCode() != 200) {
			throw new IOException();
		}
		BufferedReader responseReader = new BufferedReader(inputStreamReader);
		this.response = readFromReader(responseReader);
		return response;
	}

	public int getErrorCode() throws IOException
	{
		return this.serviceConnection.getResponseCode();
	}

	public void disconnect()
	{
		serviceConnection.disconnect();
	}

	private String readFromReader(BufferedReader input) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = input.readLine()) != null)
		{
			System.out.println(line);
			sb.append(line);
		}
		return sb.toString();
	}
}
