package pl.kleszczow.virustotalscanner.api;

public class VirusTotalAPI
{
	private final static String API_KEY = "e187a75c647fe939c4bac77126b09d35eb52f9597ad7cc02e26214db9e97616f";

	public static VirusTotalPreparedStatement prepareStatement(VirusTotalStatementType type)
	{
		return type.getStatement();
	}

	public static VirusTotalResult<?> processStatement(VirusTotalPreparedStatement<? super Object> statement, Object value)
	{
		return statement.process(value, API_KEY);
	}
}
