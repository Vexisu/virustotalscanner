package pl.kleszczow.virustotalscanner.api;

public enum VirusTotalErrorCode
{
	REQUEST_LIMIT(204),
	BAD_REQUEST(400),
	FORBIDDEN_ACCESS(403),
	NO_CONNECTION(-1);

	private final int errorCode;

	VirusTotalErrorCode(int errorCode)
	{
		this.errorCode = errorCode;
	}

	public int value()
	{
		return errorCode;
	}

	public static VirusTotalErrorCode getByValue(int errorCode)
	{
		for (VirusTotalErrorCode errorCodeEnum : values())
		{
			if (errorCodeEnum.value() == errorCode)
			{
				return errorCodeEnum;
			}
		}
		return null;
	}

	@Override
	public String toString()
	{
		return "VirusTotalErrorCode{" + "errorCode=" + errorCode + '}';
	}
}
