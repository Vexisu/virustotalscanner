package pl.kleszczow.virustotalscanner.api;

public interface VirusTotalResult<T>
{
	T value();
}
