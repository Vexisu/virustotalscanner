package pl.kleszczow.virustotalscanner.api;

public interface VirusTotalPreparedStatement<T>
{
	VirusTotalResult process(T value, String apiKey);
}
