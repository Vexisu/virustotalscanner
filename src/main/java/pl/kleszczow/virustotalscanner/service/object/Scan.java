package pl.kleszczow.virustotalscanner.service.object;

public class Scan
{
	private String antivirus;
	private String result;

	public Scan(String antivirus, String result)
	{
		this.antivirus = antivirus;
		this.result = result;
	}

	public String getAntivirus()
	{
		return antivirus;
	}

	public String getResult()
	{
		return result;
	}

	@Override
	public String toString()
	{
		return "Scan{" + "antivirus='" + antivirus + '\'' + ", result='" + result + '\'' + '}';
	}
}
