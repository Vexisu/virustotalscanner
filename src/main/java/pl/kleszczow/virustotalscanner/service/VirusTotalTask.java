package pl.kleszczow.virustotalscanner.service;

import java.io.File;
import javafx.concurrent.Task;
import pl.kleszczow.virustotalscanner.api.VirusTotalAPI;
import pl.kleszczow.virustotalscanner.api.VirusTotalPreparedStatement;
import pl.kleszczow.virustotalscanner.api.VirusTotalResult;
import pl.kleszczow.virustotalscanner.api.VirusTotalStatementType;
import pl.kleszczow.virustotalscanner.gui.SceneHandler;
import pl.kleszczow.virustotalscanner.gui.VirusTotalApplication;

public class VirusTotalTask
{
	private static VirusTotalTask instance = new VirusTotalTask();
	private Task<VirusTotalResult> task;
	private String fileName;

	public Task<VirusTotalResult> get()
	{
		return this.task;
	}

	public void runTask()
	{
		new Thread(this.task).start();
	}

	public void prepareScanTask(File file)
	{
		this.fileName = file.getName();
		Task<VirusTotalResult> scanTask = new Task<VirusTotalResult>()
		{
			@Override
			protected VirusTotalResult call()
			{
				String checksum = null;
				try
				{
					checksum = ChecksumGenerator.getMD5Checksum(file.getAbsolutePath());
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				VirusTotalPreparedStatement preparedStatement = VirusTotalAPI.prepareStatement(VirusTotalStatementType.MD5Scan);
				return VirusTotalAPI.processStatement(preparedStatement, checksum);
			}
		};
		scanTask.setOnSucceeded(event -> VirusTotalApplication.setScene(SceneHandler.RESULT, true));
		this.task = scanTask;
	}

	public String getFileName()
	{
		return fileName;
	}

	public static VirusTotalTask getInstance()
	{
		return instance;
	}
}
