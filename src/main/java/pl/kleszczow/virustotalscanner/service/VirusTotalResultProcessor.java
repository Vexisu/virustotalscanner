package pl.kleszczow.virustotalscanner.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import pl.kleszczow.virustotalscanner.service.object.Scan;

public class VirusTotalResultProcessor
{
	private static VirusTotalResultProcessor instance = new VirusTotalResultProcessor();
	private JsonObject jsonObject;

	public void parse(String json)
	{
		JsonElement jsonElement = new JsonParser().parse(json);
		this.jsonObject = jsonElement.getAsJsonObject();
	}

	public boolean isInDatabase()
	{
		int response_code = this.jsonObject.get("response_code").getAsInt();
		return response_code == 1;
	}

	public boolean isDetected()
	{
		int positives = this.jsonObject.get("positives").getAsInt();
		return positives > 0;
	}

	public List<Scan> getScansAsList()
	{
		List<Scan> scans = new ArrayList<>();
		for (Entry<String, JsonElement> entry : this.jsonObject.getAsJsonObject("scans").entrySet())
		{
			String antivirus = entry.getKey();
			JsonElement jsonValue = entry.getValue().getAsJsonObject().get("result");
			if (jsonValue instanceof JsonPrimitive)
			{
				scans.add(new Scan(antivirus, jsonValue.getAsString()));
			}
		}
		return scans;
	}

	public static VirusTotalResultProcessor getInstance()
	{
		return instance;
	}
}
