package pl.kleszczow.virustotalscanner.gui;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import pl.kleszczow.virustotalscanner.service.VirusTotalTask;

public class VirusTotalApplication extends Application
{
	private static Stage stage;

	public void start(Stage primaryStage) throws IOException
	{
		stage = primaryStage;
		List<String> parametersList = getParameters().getRaw();

		if (!parametersList.isEmpty() && new File(String.join(" ", parametersList)).isFile())
		{
			File file = new File(String.join(" ", parametersList));
			VirusTotalTask scanTask = VirusTotalTask.getInstance();
			scanTask.prepareScanTask(file);
			scanTask.runTask();
			setScene(SceneHandler.SCAN, false);
		}
		else
		{
			setScene(SceneHandler.DRAG_N_DROP, false);
		}
		primaryStage.setTitle("VirusTotal Scanner");
		primaryStage.getIcons().add(new Image("images/magnifier.png"));
		primaryStage.sizeToScene();
		primaryStage.show();
	}

	public static void setScene(SceneHandler scene, boolean resizable)
	{
		try
		{
			stage.setScene(scene.getScene());
			stage.setResizable(resizable);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		launch(args);
	}
}
