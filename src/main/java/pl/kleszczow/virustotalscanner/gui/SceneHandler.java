package pl.kleszczow.virustotalscanner.gui;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;

public enum SceneHandler
{
	DRAG_N_DROP
	{
		@Override
		public Scene getScene() throws IOException
		{
			Parent root = FXMLLoader.load(getClass().getResource("/gui/DragNDropView.fxml"));
			Scene scene = new Scene(root);
			return scene;
		}
	},
	SCAN{
		@Override
		public Scene getScene() throws IOException
		{
			Parent root = FXMLLoader.load(getClass().getResource("/gui/ScanView.fxml"));
			Scene scene = new Scene(root);
			return scene;
		}
	},
	RESULT{
		@Override
		public Scene getScene() throws IOException
		{
			Parent root = FXMLLoader.load(getClass().getResource("/gui/ResultView.fxml"));
			Scene scene = new Scene(root);
			return scene;
		}
	};

	public abstract Scene getScene() throws IOException;
}
