package pl.kleszczow.virustotalscanner.gui.dragndrop;

import java.io.File;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.input.DragEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import pl.kleszczow.virustotalscanner.gui.SceneHandler;
import pl.kleszczow.virustotalscanner.gui.VirusTotalApplication;
import pl.kleszczow.virustotalscanner.service.VirusTotalTask;

public class DragNDropController
{
	@FXML private Pane root;
	@FXML private Label dragndrop;

	@FXML
	private void initialize()
	{
		this.root.setOnDragOver(event ->
		{
			this.onDragOver(event);
			dragndrop.setText("Upuść tutaj...");
		});
		this.root.setOnDragDropped(this::onDragDropped);
	}

	@FXML
	private void onDragExited()
	{
		dragndrop.setText("Przeciągnij i upuść plik, aby przeskanować.");
	}

	private void onDragOver(DragEvent event)
	{
		event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
	}

	private void onDragDropped(DragEvent event)
	{
		File file = null;
		for (File f : event.getDragboard().getFiles())
		{
			file = f;
		}
		if (file.isFile()){
			VirusTotalTask scanTask = VirusTotalTask.getInstance();
			scanTask.prepareScanTask(file);
			scanTask.runTask();
			VirusTotalApplication.setScene(SceneHandler.SCAN, false);
		}
	}
}
