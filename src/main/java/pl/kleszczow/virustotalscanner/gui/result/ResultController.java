package pl.kleszczow.virustotalscanner.gui.result;

import java.util.List;
import com.sun.media.jfxmedia.logging.Logger;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import pl.kleszczow.virustotalscanner.api.VirusTotalErrorCode;
import pl.kleszczow.virustotalscanner.api.VirusTotalResult;
import pl.kleszczow.virustotalscanner.api.impl.result.VirusTotalErrorResult;
import pl.kleszczow.virustotalscanner.api.impl.result.VirusTotalValidResult;
import pl.kleszczow.virustotalscanner.service.VirusTotalResultProcessor;
import pl.kleszczow.virustotalscanner.service.VirusTotalTask;
import pl.kleszczow.virustotalscanner.service.object.Scan;

public class ResultController
{
	@FXML private TableView<Scan> scanLogs;
	@FXML private TableColumn<Scan, String> antivirus;
	@FXML private TableColumn<Scan, String> result;
	@FXML private Label statuslabel;
	@FXML private ImageView statusimage;
	@FXML private Tab infotab;

	@FXML
	private void initialize()
	{
		Task<VirusTotalResult> resultTask = VirusTotalTask.getInstance().get();
		VirusTotalResult scanResult = resultTask.getValue();
		if (scanResult instanceof VirusTotalValidResult)
		{
			VirusTotalResultProcessor processor = VirusTotalResultProcessor.getInstance();
			processor.parse(((VirusTotalValidResult) scanResult).value());
			this.processIndication(processor, VirusTotalTask.getInstance().getFileName());
		}
		if (scanResult instanceof VirusTotalErrorResult)
		{
			VirusTotalErrorCode errorCode = ((VirusTotalErrorResult) scanResult).value();
			this.statuslabel.setText("Nastąpił problem z połączeniem: \n" + errorCode.toString() + ": " + errorCode.name() );
			Logger.logMsg(Logger.WARNING, scanResult.value().toString());
			this.setStatusImage("cross2.png");
		}
	}

	private void processIndication(VirusTotalResultProcessor processor, String fileName)
	{

		if (processor.isInDatabase())
		{
			if (fileName.length() > 28)
			{
				fileName = fileName.substring(0, Math.min(fileName.length(), 25)) + "...";
			}
			this.processTables(processor.getScansAsList());
			if (processor.isDetected())
			{
				this.statuslabel.setText("Plik \"" + fileName + "\" jest zainfekowany");
				this.setStatusImage("cross.png");
				this.infotab.setDisable(false);
			}
			else
			{
				this.statuslabel.setText("Plik \"" + fileName + "\" jest bezpieczny");
				this.setStatusImage("tick.png");
			}
		}
		else
		{
			if (fileName.length() > 23)
			{
				fileName = fileName.substring(0, Math.min(fileName.length(), 20)) + "...";
			}
			this.statuslabel.setText("Pliku \"" + fileName + "\" nie znaleziono w bazie VirusTotal");
			this.setStatusImage("questionmark.png");
		}
	}

	private void setStatusImage(String image)
	{
		Image imageFile = new Image("images/" + image);
		this.statusimage.setImage(imageFile);
	}

	private void processTables(List<Scan> scans)
	{
		this.antivirus.setCellValueFactory(new PropertyValueFactory<>("antivirus"));
		this.result.setCellValueFactory(new PropertyValueFactory<>("result"));
		this.scanLogs.getItems().setAll(scans);
		this.scanLogs.getSortOrder().add(this.antivirus);
	}
}
