package pl.kleszczow.virustotalscanner.gui.scan;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import pl.kleszczow.virustotalscanner.service.VirusTotalTask;

public class ScanController
{
	@FXML private Label info;

	@FXML
	private void initialize()
	{
		VirusTotalTask virusTotalTask = VirusTotalTask.getInstance();
		String fileName = virusTotalTask.getFileName();
		if (fileName.length() > 35)
		{
			fileName = fileName.substring(0, Math.min(fileName.length(), 32)) + "...";
		}

		info.setText("Skanowanie pliku \"" + fileName +"\"");
	}
}
